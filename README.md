# docker-ssh

Build a new image using Dockerfile

Launch the container in a detached mode

docker run -itd "imagename" 

Now ssh the container root@containerip. It will prompt for the password.

You can also ssh it from another container if your container has ssh client on it.

Passwordless Authentication:-

##Dockerfile

FROM docker.io/centos

RUN yum install openssh-server -y

RUN /usr/bin/ssh-keygen -A

RUN mkdir /root/.ssh

RUN sed -i 's/#PermitRootLogiyes/PermitRootLogin yes/' /etc/ssh/sshd_config

RUN echo "supp0rt" | passwd --stdin root

ENTRYPOINT ["/usr/sbin/sshd"]

CMD ["-D"]

Now copy the publickey of the host to one file & mount that file to the container using -v option on /root/.ssh location of the container. 

Ex:- docker run -itd -v /tmp/authorized_keys:/root/.ssh "imagename"