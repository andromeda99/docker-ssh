FROM docker.io/centos

RUN yum install openssh-server -y

RUN /usr/bin/ssh-keygen -A

RUN sed -i 's/#PermitRootLogiyes/PermitRootLogin yes/' /etc/ssh/sshd_config

RUN echo "supp0rt" | passwd --stdin root

ENTRYPOINT ["/usr/sbin/sshd"]

CMD ["-D"]